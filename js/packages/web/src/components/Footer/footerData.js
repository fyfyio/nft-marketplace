export const footerConf = {
  showShopName: true,
  showEmailSubscriber: true,
  emailSubscriberText:
    'Join our mailing list for updates about our artists and more.',
  components: [
    {
      title: 'Company',
      links: [
        {
          label: 'About',
          url: 'www.nft.fyfy.io',
        },
        {
          label: 'Terms of service',
          url: 'www.nft.fyfy.io',
        },
      ],
    },
    {
      title: 'Help',
      links: [
        {
          label: 'FAQ',
          url: 'www.nft.fyfy.io',
        },
        {
          label: 'Support',
          url: 'support@fyfy.io',
        },
        {
          label: 'Privacy policy',
          url: 'www.nft.fyfy.io',
        },
        {
          label: 'Your purchases',
          url: 'www.marketplace.fyfy.io',
        },
      ],
    },
    {
      title: 'Follow us',
      links: [
        {
          label: 'Instagram',
          url: 'www.instagram.com/fyfyio',
        },
        {
          label: 'Twitter',
          url: 'www.twitter.com/fyfyio',
        },
        {
          label: 'Discord',
          url: 'https://discord.gg/xAGrEUHcga',
        },
      ],
    },
  ],
};
